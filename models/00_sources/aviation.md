{% docs source_aviation_airports %}
Each row in this dataset represents the record for a single airport. The primary key for interoperability purposes with other datasets is ident, but the actual internal OurAirports primary key is id. iso_region is a foreign key into the regions table.
{% enddocs %}
{% docs source_aviation_airports_id %}
Internal OurAirports integer identifier for the airport. This will stay persistent, even if the airport code changes.
{% enddocs %}
{% docs source_aviation_airports_ident %}
The text identifier used in the OurAirports URL. This will be the ICAO code if available. Otherwise, it will be a local airport code (if no conflict), or if nothing else is available, an internally-generated code starting with the ISO2 country code, followed by a dash and a four-digit number.
{% enddocs %}
{% docs source_aviation_airports_type %}
The type of the airport. Allowed values are "closed_airport", "heliport", "large_airport", "medium_airport", "seaplane_base", and "small_airport". See the map legend for a definition of each type.
{% enddocs %}
{% docs source_aviation_airports_name %}
The official airport name, including "Airport", "Airstrip", etc.
{% enddocs %}
{% docs source_aviation_airports_latitude_deg %}
The airport latitude in decimal degrees (positive for north).
{% enddocs %}
{% docs source_aviation_airports_longitude_deg %}
The airport longitude in decimal degrees (positive for east).
{% enddocs %}
{% docs source_aviation_airports_elevation_ft %}
The airport elevation MSL in feet (not metres).
{% enddocs %}
{% docs source_aviation_airports_continent %}
The code for the continent where the airport is (primarily) located. Allowed values are "AF" (Africa), "AN" (Antarctica), "AS" (Asia), "EU" (Europe), "NA" (North America), "OC" (Oceania), or "SA" (South America).
{% enddocs %}
{% docs source_aviation_airports_iso_country %}
The two-character ISO 3166:1-alpha2 code for the country where the airport is (primarily) located. A handful of unofficial, non-ISO codes are also in use, such as "XK" for Kosovo. Points to the code column in countries.
{% enddocs %}
{% docs source_aviation_airports_iso_region %}
An alphanumeric code for the high-level administrative subdivision of a country where the airport is primarily located (e.g. province, governorate), prefixed by the ISO2 country code and a hyphen. OurAirports uses ISO 3166:2 codes whenever possible, preferring higher administrative levels, but also includes some custom codes. See the documentation for regions.
{% enddocs %}
{% docs source_aviation_airports_municipality %}
The primary municipality that the airport serves (when available). Note that this is not necessarily the municipality where the airport is physically located.
{% enddocs %}
{% docs source_aviation_airports_scheduled_service %}
"yes" if the airport currently has scheduled airline service; "no" otherwise.
{% enddocs %}
{% docs source_aviation_airports_gps_code %}
The code that an aviation GPS database (such as Jeppesen's or Garmin's) would normally use for the airport. This will always be the ICAO code if one exists. Note that, unlike the ident column, this is not guaranteed to be globally unique.
{% enddocs %}
{% docs source_aviation_airports_iata_code %}
The three-letter IATA code for the airport (if it has one).
{% enddocs %}
{% docs source_aviation_airports_local_code %}
The local country code for the airport, if different from the gps_code and iata_code fields (used mainly for US airports).
{% enddocs %}
{% docs source_aviation_airports_home_link %}
URL of the airport's official home page on the web, if one exists.
{% enddocs %}
{% docs source_aviation_airports_wikipedia_link %}
URL of the airport's page on Wikipedia, if one exists.
{% enddocs %}
{% docs source_aviation_airports_keywords %}
Extra keywords/phrases to assist with search, comma-separated. May include former names for the airport, alternate codes, names in other languages, nearby tourist destinations, etc.
{% enddocs %}

{% docs source_aviation_airport_frequencies %}
Each row in this dataset represents a single airport radio frequency for voice communication (radio navigation aids appear in navaids.csv). The column airport_ident is a foreign key referencing the ident column in airports for the associated airport.
{% enddocs %}
{% docs source_aviation_airport_frequencies_id %}
Internal OurAirports integer identifier for the frequency. This will stay persistent, even if the radio frequency or description changes.
{% enddocs %}
{% docs source_aviation_airport_frequencies_airport_ref %}
Internal integer foreign key matching the id column for the associated airport in airports. (airport_ident is a better alternative.)
{% enddocs %}
{% docs source_aviation_airport_frequencies_airport_ident %}
Externally-visible string foreign key matching the ident column for the associated airport in airports.
{% enddocs %}
{% docs source_aviation_airport_frequencies_type %}
A code for the frequency type. This isn't (currently) a controlled vocabulary, but probably will be soon. Some common values are "TWR" (tower), "ATF" or "CTAF" (common traffic frequency), "GND" (ground control), "RMP" (ramp control), "ATIS" (automated weather), "RCO" (remote radio outlet), "ARR" (arrivals), "DEP" (departures), "UNICOM" (monitored ground station), and "RDO" (a flight-service station).
{% enddocs %}
{% docs source_aviation_airport_frequencies_description %}
A description of the frequency, typically the way a pilot would open a call on it.
{% enddocs %}
{% docs source_aviation_airport_frequencies_frequency_mhz %}
Radio voice frequency in megahertz. Note that the same frequency may appear multiple times for an airport, serving different functions.
{% enddocs %}

{% docs source_aviation_runways %}
Each row in this dataset represents a single airport landing surface (runway, helipad, or waterway). The initial fields apply to the entire surface, in both directions. Fields beginning with le_* apply only to the low-numbered end of the runway (e.g. 09), while fields beginning with he_* apply only to the high-numbered end of the runway (e.g. 27).
{% enddocs %}
{% docs source_aviation_runways_id %}
Internal OurAirports integer identifier for the runway. This will stay persistent, even if the runway numbering changes.
{% enddocs %}
{% docs source_aviation_runways_airport_ref %}
Internal integer foreign key matching the id column for the associated airport in airports. (airport_ident is a better alternative.)
{% enddocs %}
{% docs source_aviation_runways_airport_ident %}
Externally-visible string foreign key matching the ident column for the associated airport in airports.
{% enddocs %}
{% docs source_aviation_runways_length_ft %}
Length of the full runway surface (including displaced thresholds, overrun areas, etc) in feet.
{% enddocs %}
{% docs source_aviation_runways_width_ft %}
Width of the runway surface in feet.
{% enddocs %}
{% docs source_aviation_runways_surface %}
Code for the runway surface type. This is not yet a controlled vocabulary, but probably will be soon. Some common values include "ASP" (asphalt), "TURF" (turf), "CON" (concrete), "GRS" (grass), "GRE" (gravel), "WATER" (water), and "UNK" (unknown).
{% enddocs %}
{% docs source_aviation_runways_lighted %}
1 if the surface is lighted at night, 0 otherwise. (Note that this is inconsistent with airports.csv, which uses "yes" and "no" instead of 1 and 0.)
{% enddocs %}
{% docs source_aviation_runways_closed %}
1 if the runway surface is currently closed, 0 otherwise.
{% enddocs %}
{% docs source_aviation_runways_le_ident %}
Identifier for the low-numbered end of the runway.
{% enddocs %}
{% docs source_aviation_runways_le_latitude_deg %}
Latitude of the centre of the low-numbered end of the runway, in decimal degrees (positive is north), if available.
{% enddocs %}
{% docs source_aviation_runways_le_longitude_deg %}
Longitude of the centre of the low-numbered end of the runway, in decimal degrees (positive is east), if available.
{% enddocs %}
{% docs source_aviation_runways_le_elevation_ft %}
Elevation above MSL of the low-numbered end of the runway in feet.
{% enddocs %}
{% docs source_aviation_runways_le_heading_degt %}
Heading of the low-numbered end of the runway in degrees true (not magnetic).
{% enddocs %}
{% docs source_aviation_runways_le_displaced_threshold_ft %}
Length of the displaced threshold (if any) for the low-numbered end of the runway, in feet.
{% enddocs %}
{% docs source_aviation_runways_he_ident %}
Identifier for the high-numbered end of the runway.
{% enddocs %}
{% docs source_aviation_runways_he_latitude_deg %}
Latitude of the centre of the high-numbered end of the runway, in decimal degrees (positive is north), if available.
{% enddocs %}
{% docs source_aviation_runways_he_longitude_deg %}
Longitude of the centre of the high-numbered end of the runway, in decimal degrees (positive is east), if available.
{% enddocs %}
{% docs source_aviation_runways_he_elevation_ft %}
Elevation above MSL of the high-numbered end of the runway in feet.
{% enddocs %}
{% docs source_aviation_runways_he_heading_degt %}
Heading of the high-numbered end of the runway in degrees true (not magnetic).
{% enddocs %}
{% docs source_aviation_runways_he_displaced_threshold_ft %}
Length of the displaced threshold (if any) for the high-numbered end of the runway, in feet.
{% enddocs %}

{% docs source_aviation_navaids %}
Each row in this dataset represents a single radio navigation. When the navaid is associated with an airport, the associated_airport field links to the ident field in airports.
{% enddocs %}
{% docs source_aviation_navaids_id %}
Internal OurAirports integer identifier for the navaid. This will stay persistent, even if the navaid identifier or frequency changes.
{% enddocs %}
{% docs source_aviation_navaids_filename %}
This is a unique string identifier constructed from the navaid name and country, and used in the OurAirports URL.
{% enddocs %}
{% docs source_aviation_navaids_ident %}
The 1-3 character identifer that the navaid transmits.
{% enddocs %}
{% docs source_aviation_navaids_name %}
The name of the navaid, excluding its type.
{% enddocs %}
{% docs source_aviation_navaids_type %}
The type of the navaid. Options are "DME", "NDB", "NDB-DME", "TACAN", "VOR", "VOR-DME", or "VORTAC". See the map legend for more information about each type.
{% enddocs %}
{% docs source_aviation_navaids_frequency_khz %}
The frequency of the navaid in kilohertz. If the Navaid operates on the VHF band (VOR, VOR-DME) or operates on the UHF band with a paired VHF frequency (DME, TACAN, VORTAC), the you need to divide this number by 1,000 to get the frequency in megahertz (115.3 MHz in this example). For an NDB or NDB-DME, you can use this frequency directly.
{% enddocs %}
{% docs source_aviation_navaids_latitude_deg %}
The latitude of the navaid in decimal degrees (negative for south).
{% enddocs %}
{% docs source_aviation_navaids_longitude_deg %}
The longitude of the navaid in decimal degrees (negative for west).
{% enddocs %}
{% docs source_aviation_navaids_elevation_ft %}
The navaid's elevation MSL in feet (not metres).
{% enddocs %}
{% docs source_aviation_navaids_iso_country %}
The two-character ISO 3166:1-alpha2 code for the country that operates the navaid. A handful of unofficial, non-ISO codes are also in use, such as "XK" for Kosovo.
{% enddocs %}
{% docs source_aviation_navaids_dme_frequency_khz %}
The paired VHF frequency for the DME (or TACAN) in kilohertz. Divide by 1,000 to get the paired VHF frequency in megahertz (e.g. 115.3 MHz).
{% enddocs %}
{% docs source_aviation_navaids_dme_channel %}
The DME channel (an alternative way of tuning distance-measuring equipment).
{% enddocs %}
{% docs source_aviation_navaids_dme_latitude_deg %}
The latitude of the associated DME in decimal degrees (negative for south). If missing, assume that the value is the same as latitude_deg.
{% enddocs %}
{% docs source_aviation_navaids_dme_longitude_deg %}
The longitude of the associated DME in decimal degrees (negative for west). If missing, assume that the value is the same as longitude_deg.
{% enddocs %}
{% docs source_aviation_navaids_dme_elevation_ft %}
The associated DME transmitters elevation MSL in feet. If missing, assume that it's the same value as elevation_ft.
{% enddocs %}
{% docs source_aviation_navaids_slaved_variation_deg %}
The magnetic variation adjustment built into a VOR's, VOR-DME's, or TACAN's radials. Positive means east (added to the true direction), and negative means west (subtracted from the true direction). This will not usually be the same as magnetic-variation because the magnetic pole is constantly in motion.
{% enddocs %}
{% docs source_aviation_navaids_magnetic_variation_deg %}
The actual magnetic variation at the navaid's location. Positive means east (added to the true direction), and negative means west (subtracted from the true direction).
{% enddocs %}
{% docs source_aviation_navaids_usagetype %}
The primary function of the navaid in the airspace system. Options include "HI" (high-altitude airways, at or above flight level 180), "LO" (low-altitude airways), "BOTH" (high- and low-altitude airways), "TERM" (terminal-area navigation only), and "RNAV" (non-GPS area navigation).
{% enddocs %}
{% docs source_aviation_navaids_power %}
The power-output level of the navaid. Options include "HIGH", "MEDIUM", "LOW", and "UNKNOWN".
{% enddocs %}
{% docs source_aviation_navaids_associated_airport %}
The OurAirports text identifier (usually the ICAO code) for an airport associated with the navaid. Links to the ident column in airports.
{% enddocs %}