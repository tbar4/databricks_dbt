{% docs source_geographical %}
The tables represent geographical data.
{% enddocs %}
{% docs source_geographical_countries %}
Each row represents a country or country-like entity (e.g. Hong Kong). The iso_country column in airports, navaids, and regions refer to the code column here.
{% enddocs %}
{% docs source_geographical_countries_id %}
Internal OurAirports integer identifier for the country. This will stay persistent, even if the country name or code changes.
{% enddocs %}
{% docs source_geographical_countries_code %}
The two-character ISO 3166:1-alpha2 code for the country. A handful of unofficial, non-ISO codes are also in use, such as "XK" for Kosovo. The iso_country field in airports points into this column.
{% enddocs %}
{% docs source_geographical_countries_name %}
The common English-language name for the country. Other variations of the name may appear in the keywords field to assist with search.
{% enddocs %}
{% docs source_geographical_countries_continent %}
The code for the continent where the country is (primarily) located. See the continent code in airports for allowed values.
{% enddocs %}
{% docs source_geographical_countries_wikipedia_link %}
Link to the Wikipedia article about the country.
{% enddocs %}
{% docs source_geographical_countries_keywords %}
A comma-separated list of search keywords/phrases related to the country.
{% enddocs %}

{% docs source_geographical_regions %}
Each row represents a high-level administrative subdivision of a country. The iso_region column in airports links to the code column in this dataset.
{% enddocs %}
{% docs source_geographical_regions_id %}
Internal OurAirports integer identifier for the region. This will stay persistent, even if the region code changes.
{% enddocs %}
{% docs source_geographical_regions_code %}
local_code prefixed with the country code to make a globally-unique identifier.
{% enddocs %}
{% docs source_geographical_regions_local_code %}
The local code for the administrative subdivision. Whenever possible, these are official ISO 3166:2, at the highest level available, but in some cases OurAirports has to use unofficial codes. There is also a pseudo code "U-A" for each country, which means that the airport has not yet been assigned to a region (or perhaps can't be, as in the case of a deep-sea oil platform).
{% enddocs %}
{% docs source_geographical_regions_name %}
The common English-language name for the administrative subdivision. In some cases, the name in local languages will appear in the keywords field assist search.
{% enddocs %}
{% docs source_geographical_regions_continent %}
A code for the continent to which the region belongs. See the continent field in airports.csv for a list of codes.
{% enddocs %}
{% docs source_geographical_regions_iso_country %}
The two-character ISO 3166:1-alpha2 code for the country containing the administrative subdivision. A handful of unofficial, non-ISO codes are also in use, such as "XK" for Kosovo.
{% enddocs %}
{% docs source_geographical_regions_wikipedia_link %}
A link to the Wikipedia article describing the subdivision.
{% enddocs %}
{% docs source_geographical_regions_keywords %}
A comma-separated list of keywords to assist with search. May include former names for the region, and/or the region name in other languages.
{% enddocs %}