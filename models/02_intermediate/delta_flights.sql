{{ config(
  materialized='table',
  file_format='delta'
) }}

with flights_2015 as (
	select * from {{ ref('stg_flights_2015') }}
)

SELECT
*
FROM flights_2015

WHERE airline = 'DL'
