import json
import requests
import pandas as pd
import pyarrow as pa
from pyarrow import fs
import pyarrow.parquet as pq
import boto3


"""
parser = configparser.ConfigParser()
parser.read('/Users/trevorb/Documents/gitlab/databricks_dbt/pipeline.conf')
api_key = parser.get('alpha_vantage', "api_key")
"""

class AlphaVantage():
    """
    This class pulls the data from Alpha vantage than transfers it to the the appropriate parquet format to write to S3
    """
    url = "https://www.alphavantage.co/query?"

    f = open('alpha_vantage_api_calls.json')
    data = json.load(f)

    def __init__(self, tickers, api_key = "demo"):
        if type(tickers) is list:
            self.tickers = tickers
        else:
            self.tickers = [tickers]
        self.api_key = api_key

    @staticmethod
    def list_api_calls():
        """
        This function takes no arguments but simply lists the supported API calls in this module for alpha vantage
        :return: parsed json of supported alpha vantage api calls
        """
        s = open('alpha_vantage_api_calls.json')
        datum = json.load(s)

        for api in datum['Categories']:
            print(api, ":")
            for funct in datum['Categories'][api]['APIs']:
                print("\t", funct, ":")
                print("\t\t", datum['Categories'][api]['APIs'][funct]['Description'])

    def get_company_overview(self):
        api_params = self.data['Categories']['Fundamental Data']['APIs']['Company Overview']['Parameters']

        funct = api_params['Function']['Call']
        symbol = api_params['Symbol']['Call']
        apikey = api_params['API Key']['Call']

        for ticker in self.tickers:
            query_string = f"{self.url}{funct}&{symbol}{ticker}&{apikey}{self.api_key}"
            r = requests.get(query_string)
            data = r.json()
            print(data)

    def get_intraday(self, adjusted="true", interval="5min", outputsize="full", datatype="json"):
        api_params = self.data['Categories']['Time Series Stock APIs']['APIs']['Time Series Intraday']['Parameters']

        funct_call = api_params['Function']['Call']
        symbol_call = api_params['Symbol']['Call']
        interval_call = api_params['Interval']['Call']
        adjusted_call = api_params['Adjusted']['Call']
        output_size_call = api_params['Output Size']['Call']
        data_type_call = api_params['Data Type']['Call']
        apikey_call = api_params['API Key']['Call']

        df = pd.DataFrame([])

        for ticker in self.tickers:
            query_string = f"{self.url}{funct_call}&{symbol_call}{ticker}&{interval_call}{interval}&{adjusted_call}{adjusted}&{output_size_call}{outputsize}&{data_type_call}{datatype}&{apikey_call}{self.api_key}"
            r = requests.get(query_string)
            data = r.json()

            keys = pd.DataFrame({"timestamp": data[f"Time Series ({interval})"].keys()})
            values = pd.json_normalize(data[f"Time Series ({interval})"].values())
            concatted = pd.concat([keys, values], axis = 1)
            concatted.rename(columns = {"1. open" : "open", "2. high": "high",  "3. low": "low", "4. close": "close", "5. volume": "volume"})
            concatted['ticker'] = ticker
            df = df.append(concatted)

        df["timestamp"] = pd.to_datetime(df["timestamp"])
        df["year"] = df["timestamp"].dt.year
        df["month"] = df["timestamp"].dt.month
        df["day"] = df["timestamp"].dt.day

        table = pa.Table.from_pandas(df)

        pq.write_to_dataset(table, flavor = "spark", root_path='intraday', partition_cols=['ticker', 'year', 'month', 'day'])

        return table

    def write_intraday(self, table, s3_path):

        s3 = fs.S3FileSystem()
        pq.write_to_dataset(table, root_path=s3_path, partition_cols=['ticker', 'year', 'month', 'day'], filesystem=s3)

    f.close()

AlphaVantage.list_api_calls()

with open("./stocks.txt") as f:
    tickers = f.read().splitlines()

av = AlphaVantage(tickers[:2], api_key=api_key)

#av.get_company_overview()


test_table = av.get_intraday()
av.write_intraday(test_table, "datadazed/extracts/alpha_vantage/intraday/")